﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaxiBackEnd.Domain.Entities;

namespace TaxiBackEnd.Domain.Interfaces.Repositories
{
    /**
     * <summary>Interfaz que contiene los métodos para la entidad del historico, e implementa la interfaz IBaseRepository.</summary>
     */
    public interface IHistoricTripRepository: IBaseRepository<HistoricTrip>
    {
    }
}

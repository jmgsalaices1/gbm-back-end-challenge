﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaxiBackEnd.Domain.Entities;

namespace TaxiBackEnd.Domain.Interfaces.Repositories
{
    public interface ITripRepository : IBaseRepository<Trip>
    {
    }
}

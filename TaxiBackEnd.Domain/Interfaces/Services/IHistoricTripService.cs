﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaxiBackEnd.Domain.Entities;

namespace TaxiBackEnd.Domain.Interfaces.Services
{
    /// <summary>
    /// Interfaz que contiene los métodos para la entidad de HistoricTrip, e implementa la interfaz IBaseService.
    /// </summary>
    public interface IHistoricTripService : IBaseService<HistoricTrip>
    {
    }
}

﻿using TaxiBackEnd.Domain.Entities;
using TaxiBackEnd.Domain.Interfaces.Repositories;

namespace TaxiBackEnd.Infraestructure.Data.Repositories
{
    /// <summary>
    /// Clase que contiene los métodos de la entidad Trip, se encarga de realizar las consultas a Entity Framework.
    /// </summary>
    public class TripRepository: BaseRepository<Trip>, ITripRepository
    {
    }
}

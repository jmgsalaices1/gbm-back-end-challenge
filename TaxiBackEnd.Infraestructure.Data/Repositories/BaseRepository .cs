﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using TaxiBackEnd.Domain.Interfaces.Repositories;

namespace TaxiBackEnd.Infraestructure.Data.Repositories
{
    /// <summary>
    /// Clase genérica que puede ser utilizado por cualquier entidad, se encarga de realizar las consultas a Entity Framework.
    /// </summary>
    public class BaseRepository<TEntity> : IDisposable, IBaseRepository<TEntity> where TEntity : class
    {
        public void add(TEntity entity)
        {
            try
            {
                using (var context = new TaxidbEntities())
                {
                    context.Set<TEntity>().Add(entity);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {

                throw new Exception("No se puede guardar el registro", ex); ;
            }
        }

        public void delete(int id)
        {
            try
            {
                using (var context = new TaxidbEntities())
                {
                    var entity = context.Set<TEntity>().Find(id);
                    context.Set<TEntity>().Remove(entity);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {

                throw new Exception("No se puede eliminar el registro", ex); ;
            }
        }

        public void Dispose()
        {
            this.Dispose();
        }

        public IEnumerable<TEntity> getAll()
        {
            try
            {
                using (var context = new TaxidbEntities())
                {
                    return context.Set<TEntity>().ToList();
                }
            }
            catch (Exception ex)
            {

                throw new Exception("No se pudieron recuperar los registros", ex);
            }
        }

        public TEntity getById(int id)
        {
            try
            {
                using (var context = new TaxidbEntities())
                {
                    return context.Set<TEntity>().Find(id);
                }
            }
            catch (Exception ex)
            {

                throw new Exception("No se pudo recuperar el registro", ex);
            }
        }

        public void modify(TEntity entity)
        {
            try
            {
                using (var context = new TaxidbEntities())
                {
                    context.Entry(entity).State = EntityState.Modified;
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {

                throw new Exception("No se puede actualiz el registro", ex);
            }
        }
    }
}

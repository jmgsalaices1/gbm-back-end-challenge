﻿using TaxiBackEnd.Domain.Interfaces.Repositories;
using TaxiBackEnd.Domain.Interfaces.Services;
using TaxiBackEnd.Domain.Entities;

namespace TaxiBackEnd.Domain.Core
{
    public class TripService: BaseService<Trip>, ITripService
    {
        private readonly ITripRepository tripRepository;

        public TripService(ITripRepository tripRepository) : base(tripRepository)
        {
            this.tripRepository = tripRepository;
        }
    }
}

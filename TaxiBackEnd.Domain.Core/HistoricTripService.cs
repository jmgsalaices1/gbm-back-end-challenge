﻿using TaxiBackEnd.Domain.Interfaces.Repositories;
using TaxiBackEnd.Domain.Interfaces.Services;
using TaxiBackEnd.Domain.Entities;

namespace TaxiBackEnd.Domain.Core
{
    public class HistoricTripService : BaseService<HistoricTrip>, IHistoricTripService
    {
        private readonly IHistoricTripRepository historicTripRepository;

        public HistoricTripService(IHistoricTripRepository historicTripRepository) : base(historicTripRepository)
        {
            this.historicTripRepository = historicTripRepository;
        }
    }
}

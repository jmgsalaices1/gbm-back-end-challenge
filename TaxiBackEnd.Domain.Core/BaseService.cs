﻿using System;
using System.Collections.Generic;
using TaxiBackEnd.Domain.Interfaces.Repositories;
using TaxiBackEnd.Domain.Interfaces.Services;

namespace TaxiBackEnd.Domain.Core
{
    /// <summary>
    /// Clase genérica que puede ser utilizado por cualquier entidad, se encarga de realizar la conexión entre los servicios y los repositorios.
    /// </summary>
    /// <typeparam name="TEntity">Corresponde a la entidad con la cual van a trabajar los métodos de esta interfaz, por ejemplo: entidad de tipo “Trip”/.</typeparam>
    public class BaseService<TEntity> : IDisposable, IBaseService<TEntity> where TEntity : class
    {

        private readonly IBaseRepository<TEntity> baseRepository;

        public BaseService(IBaseRepository<TEntity> baseRepository)
        {
            this.baseRepository = baseRepository;
        }

        /// <summary>
        /// Método que permite ingresar una entidad
        /// </summary>
        /// <param name="entity">Corresponde a la entidad que se desea agregar</param>
        public void add(TEntity entity)
        {
            this.baseRepository.add(entity);
        }

        /// <summary>
        /// Método que permite eliminar una entidad
        /// </summary>
        /// <param name="id">Corresponde al identificador de la entidad que se desea eliminar</param>
        public void delete(int id)
        {
            this.baseRepository.delete(id);
        }

        public void Dispose()
        {
            this.baseRepository.Dispose();
        }

        /// <summary>
        /// Método que permite obtener todos los registros pertenecientes a esa entidad
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Todos los objetos de ese tipo de entidad</returns>
        public IEnumerable<TEntity> getAll()
        {
            return this.baseRepository.getAll();
        }

        /// <summary>
        /// Método que permite obtener la información correspondiente a la entidad solicitada
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Corresponde al identificador de ese tipo de entidad</returns>
        public TEntity getById(int id)
        {
           return this.baseRepository.getById(id);
        }

        /// <summary>
        /// Método que permite actualizar la información una entidad
        /// </summary>
        /// <param name="entity">Corresponde a la entidad que se desea modificar</param>
        public void modify(TEntity entity)
        {
            this.baseRepository.modify(entity);
        }
    }
}

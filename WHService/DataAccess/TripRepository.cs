﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Spatial;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace WHService.DataAccess
{
    public class TripRepository
    {
        public static List<Domain.Trip> GeTripList()
        {
            List<Domain.Trip> ass = new List<Domain.Trip>();
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Context"].ToString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = conn;
                        cmd.CommandText = @"SELECT * FROM [Trip]";
                        using (IDataReader r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                ass.Add(PopulateData(r));
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return ass;
        }

        public static Domain.Trip GetTripById(int TripId)
        {
            Domain.Trip a = null;
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Context"].ToString()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = @"SELECT * FROM [Trip] WHERE TripId=@TripId";
                    cmd.Parameters.AddWithValue("@TripId", TripId);
                    using (IDataReader r = cmd.ExecuteReader())
                    {
                        while (r.Read())
                        {
                            a = PopulateData(r);
                        }
                    }
                }
            }
            return a;
        }

        public static Domain.Trip PopulateData(IDataReader r)
        {
            Domain.Trip a = new Domain.Trip();
            a.TaxiId = Convert.ToInt32(r["TaxiId"]);
            a.ClientId = Convert.ToInt32(r["ClientId"]);
            a.Status = Convert.ToByte(r["Status"]);
            a.StartPoint = DbGeography.FromText(r["StartPoint"].ToString());
            a.EndPoint = DbGeography.FromText(r["EndPoint"].ToString());
            a.CreateDate = Convert.ToDateTime(r["CreateDate"]);
            a.UpdateDate = Convert.ToDateTime(r["UpdateDate"]);
            return a;
        }

        public static void Add(Domain.Trip a)
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Context"].ToString()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = @"INSERT INTO [dbo].[Trip] ([TaxiId],[ClientId],[Status],[StartPoint],[EndPoint],[CreateDate],[UpdateDate])
                                        VALUES (@TaxiId,@ClientId,@Status,@StartPoint,@EndPoint,CreateDate,UpdateDate);
                                        SELECT SCOPE_IDENTITY() AS TripId";
                    cmd.Parameters.AddWithValue("@TaxiId", a.TaxiId);
                    cmd.Parameters.AddWithValue("@ClientId", a.ClientId);
                    cmd.Parameters.AddWithValue("@Status", a.Status);
                    cmd.Parameters.AddWithValue("@StartPoint", a.StartPoint);
                    cmd.Parameters.AddWithValue("@UpdateDate", a.UpdateDate);
                    cmd.Parameters.AddWithValue("@UpdateDate", a.UpdateDate);
                    var tripSend = Convert.ToInt32(cmd.ExecuteScalar());
                }
            }
        }
       
    }
}
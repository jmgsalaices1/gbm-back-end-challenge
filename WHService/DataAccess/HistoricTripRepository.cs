﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Spatial;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace WHService.DataAccess
{
    public class HistoricTripRepository
    {
        public static List<Domain.HistoricTrip> GetHistoricTripList()
        {
            List<Domain.HistoricTrip> lst = new List<Domain.HistoricTrip>();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Context"].ToString()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = @"SELECT * FROM HistoricTrip]";
                    using (IDataReader r = cmd.ExecuteReader())
                    {
                        while (r.Read())
                        {
                            lst.Add(PopulateData(r));
                        }
                    }
                }
            }
            return lst;
        }

        public static List<Domain.HistoricTrip> GetHistorcTripById(int TripId)
        {
            List<Domain.HistoricTrip> lst = new List<Domain.HistoricTrip>();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Context"].ToString()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = @"SELECT * FROM [HistoricTrip] WHERE TripId=@TripId";
                    cmd.Parameters.AddWithValue("@TripId", TripId);
                    using (IDataReader r = cmd.ExecuteReader())
                    {
                        while (r.Read())
                        {
                            lst.Add(PopulateData(r));
                        }
                    }
                }
            }
            return lst;
        }

        public static Domain.HistoricTrip PopulateData(IDataReader r)
        {
            Domain.HistoricTrip a = new Domain.HistoricTrip();
            a.HistoricId = Convert.ToInt32(r["TripId"]);
            a.TripId = Convert.ToInt32(r["TripId"]);
            a.Location = DbGeography.FromText(r["Location"].ToString());
            a.StatusTrip = Convert.ToByte(r["StatusTrip"]);
            a.UpdateDate = DateTime.Now;

            return a;
        }

        public static void Add(Domain.HistoricTrip a)
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Context"].ToString()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = @"INSERT INTO [dbo].[HistoricTrip] ([TripId],[Location],[StatusTrip],[UpdateDate])
                                        VALUES (@TripId,@Location,@StatusTrip,@UpdateDate);
                                        SELECT SCOPE_IDENTITY() AS HistoricId";

                    cmd.Parameters.AddWithValue("@TripId", a.TripId);
                    cmd.Parameters.AddWithValue("@Location", a.Location);
                    cmd.Parameters.AddWithValue("@StatusTrip", a.StatusTrip);
                    cmd.Parameters.AddWithValue("@UpdateDate", a.UpdateDate);
                    a.HistoricId = Convert.ToInt32(cmd.ExecuteScalar());
                }
            }
        }

        public static DbGeography ConvertLatLonToDbGeography(double longitude, double latitude)
        {
            var point = string.Format("POINT({1} {0})", latitude, longitude);
            return DbGeography.FromText(point);
        }

    }

}
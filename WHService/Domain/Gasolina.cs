﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WHService.Domain
{
    public class Gasolina
    {
        public string _id { get; set; }
        public string calle { get; set; }
        public string rfc { get; set; }
        public string date_insert { get; set; }
        public double regular { get; set; }
        public string colonia { get; set; }
        public string numeropermiso { get; set; }
        public string fechaaplicacion { get; set; }
        public int permisoid { get; set; }
        public string longitude { get; set; }
        public string latitude { get; set; }
        public double premium { get; set; }
        public string razonsocial { get; set; }
        public int codigopostal { get; set; }
        public string dieasel { get; set; }
    }
}
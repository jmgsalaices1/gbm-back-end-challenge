using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WHService.Domain
{
    public class HistoricTrip
    {
        public int HistoricId { get; set; }
        public int TripId { get; set; }
        public System.Data.Entity.Spatial.DbGeography Location { get; set; }
        public Nullable<byte> StatusTrip { get; set; }
        public Nullable<System.DateTime> UpdateDate { get; set; }
    }
}


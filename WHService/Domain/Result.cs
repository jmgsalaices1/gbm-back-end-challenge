﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WHService.Domain
{
    public class Result
    {
        public bool Success { get; set; }
        public string Error { get; set; }
        public int ErrorNumber { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WHService.Contract.Data.HistoricTrip
{
    public class GetHistoricTripResponse
    {
        public int HistoricId { get; set; }
        public int TripId { get; set; }
        public System.Data.Entity.Spatial.DbGeography Location { get; set; }
        public Nullable<byte> StatusTrip { get; set; }
        public Nullable<System.DateTime> UpdateDate { get; set; }
    }
}
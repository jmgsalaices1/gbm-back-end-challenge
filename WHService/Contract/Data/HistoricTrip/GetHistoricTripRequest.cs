﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WHService.Contract.Data.HistoricTrip
{
    public class GetHistoricTripRequest
    {
        public int TripId { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WHService.Contract.Data.Trip
{
    public class GetTripRequest
    {
        public int TripId { get; set; }
    }
}
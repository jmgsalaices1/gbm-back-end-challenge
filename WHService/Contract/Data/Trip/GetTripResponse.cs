﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WHService.Contract.Data.Trip
{
    public class GetTripResponse
    {
        public int TripId { get; set; }
        public int TaxiId { get; set; }
        public int ClientId { get; set; }
        public Nullable<byte> Status { get; set; }
        public System.Data.Entity.Spatial.DbGeography StartPoint { get; set; }
        public System.Data.Entity.Spatial.DbGeography EndPoint { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public Nullable<System.DateTime> UpdateDate { get; set; }
    }
}
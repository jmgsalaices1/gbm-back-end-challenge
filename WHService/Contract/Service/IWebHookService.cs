﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Web;
using WHService.Contract.Data.HistoricTrip;
using WHService.Contract.Data.Trip;

namespace WHService.Contract.Service
{   
    [ServiceContract]
    public interface IWebHookService : IDisposable
    {
        [OperationContract]
        [WebInvoke(UriTemplate = "{HistoricTrip}/{id=0}/", Method = "GET", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json),
        Description("Obtiene Historico de Viajes Generado")]
        GetHistoricTripsResponse GetHistoricTrip(string id);

        [OperationContract]
        [WebInvoke(UriTemplate = "{Trip}/{id=0}/", Method = "GET", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json),
        Description("Obtiene Viajes Generados")]
        GetTripsResponse GetTrip(string id);

        [OperationContract]
        [WebInvoke(UriTemplate = "{HistoricTrip}/", Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json),
        Description("Historico Generado")]
        AddHistoricTripResponse PostHistoricTrip(AddHistoricTripRequest request);

        [OperationContract]
        [WebInvoke(UriTemplate = "{Trip}/", Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json),
        Description("Viaje Generado")]
        AddTripResponse PostTrip(AddTripRequest request);
    }
    
}
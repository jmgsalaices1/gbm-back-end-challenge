﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Web;
using WHService.Contract.Data.HistoricTrip;
using WHService.Contract.Data.Trip;

namespace WHService.Contract.Service
{
    [ServiceContract]
    public interface IWebHookManager : IDisposable
    {
        [OperationContract]
        GetHistoricTripsResponse GetHistoricTrips(GetHistoricTripRequest request);
        [OperationContract]
        GetHistoricTripsResponse GetHistoricTrip(GetHistoricTripRequest request);
        [OperationContract]
        AddHistoricTripResponse AddHistoricTrip(AddHistoricTripRequest request);
        [OperationContract]
        GetTripsResponse GetTrips(GetTripRequest request);
        [OperationContract]
        GetTripResponse GetTrip(GetTripRequest request);
        [OperationContract]
        AddTripResponse AddTrip(AddTripRequest request);
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Web;
using System.Web;
using WHService.Application;
using WHService.Contract.Data.HistoricTrip;
using WHService.Contract.Data.Trip;
using WHService.Contract.Service;

namespace WHService.Service
{
    public class WebHookService : IWebHookService
    {
        private readonly AppManager appwh;
        public WebHookService()
        {
            appwh = new AppManager();
        }

        public GetHistoricTripsResponse GetHistoricTrip(string id)
        {
            GetHistoricTripsResponse response = new GetHistoricTripsResponse();
            try
            {
                //System.Net.WebHeaderCollection headers = ValidateHeaders();
                GetHistoricTripRequest request = new GetHistoricTripRequest();
                request.TripId = string.IsNullOrEmpty(id) ? 0 : Convert.ToInt32(id);
                response = appwh.GetHistoricTripByTripId(request);
            }
            catch (Exception ex)
            {
                throw new WebFaultException(System.Net.HttpStatusCode.BadRequest);
            }
            return response;
        }

        public GetTripsResponse GetTrip(string id)
        {
            GetTripsResponse response = new GetTripsResponse();
            try
            {
                
                GetTripRequest request = new GetTripRequest();
                request.TripId = string.IsNullOrEmpty(id) ? 0 : Convert.ToInt32(id);
                response = appwh.GetTrips(request);
            }
            catch (Exception ex)
            {
                throw new WebFaultException(System.Net.HttpStatusCode.BadRequest);
            }
            return response;
        }

        public AddHistoricTripResponse PostHistoricTrip(AddHistoricTripRequest request)
        {
            AddHistoricTripResponse response = new AddHistoricTripResponse();
            try
            {
                response = appwh.AddHistoricTrip(request);
            }
            catch (Exception ex)
            {
                throw new WebFaultException(System.Net.HttpStatusCode.BadRequest);
            }
            return response;
        }

        public AddTripResponse PostTrip(AddTripRequest request)
        {
            AddTripResponse response = new AddTripResponse();
            try
            {
                response = appwh.AddTrip(request);
            }
            catch (Exception ex)
            {
                throw new WebFaultException(System.Net.HttpStatusCode.BadRequest);
            }
            return response;
        }

        public void Dispose()
        {
        }
    }
}
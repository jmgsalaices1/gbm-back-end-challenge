﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WHService.Application;
using WHService.Contract.Data.HistoricTrip;
using WHService.Contract.Data.Trip;
using WHService.Contract.Service;

namespace WHService.Service
{
    public class WebHookManager : IWebHookManager
    {
        private readonly AppManager _app;
        public WebHookManager()
        {
            this._app = new AppManager();
        }
        public AddHistoricTripResponse AddHistoricTrip(AddHistoricTripRequest request)
        {
            return this._app.AddHistoricTrip(request);
        }

        public AddTripResponse AddTrip(AddTripRequest request)
        {
            return this._app.AddTrip(request);
        }

        public GetHistoricTripsResponse GetHistoricTrip(GetHistoricTripRequest request)
        {
            return this._app.GetHistoricTripByTripId(request);
        }

        public GetHistoricTripsResponse GetHistoricTrips(GetHistoricTripRequest request)
        {
            return this._app.GetHistoricTrips(request);
        }


        public GetTripResponse GetTrip(GetTripRequest request)
        {
            return this._app.GetTrip(request);
        }

        public GetTripsResponse GetTrips(GetTripRequest request)
        {
            return this._app.GetTrips(request);
        }



        public void Dispose()
        {
        }

    }
}
﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WHService.Contract.Data.HistoricTrip;
using WHService.Contract.Data.Trip;

namespace WHService.Application
{
    public class AppProfile
    {
        public static IMapper ConfigMaps()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Domain.HistoricTrip, GetHistoricTripResponse>();
                cfg.CreateMap<Domain.Trip, GetTripResponse>();
            });
            return config.CreateMapper();
        }
    }
}
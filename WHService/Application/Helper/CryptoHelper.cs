﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace WHService.Application.Helper
{
    public partial class CryptoHelper
    {
        public RSACryptoServiceProvider RSAService { get; set; }

        public CryptoHelper()
        {
            this.RSAService = new RSACryptoServiceProvider(2048);
        }

        public byte[] CreatePublicKey()
        {
            string xmlPublicKey = this.RSAService.ToXmlString(false);
            return Encoding.ASCII.GetBytes(xmlPublicKey);
        }
        public byte[] CreatePrivateKey()
        {
            string xmlPrivateKey = this.RSAService.ToXmlString(true);
            return Encoding.ASCII.GetBytes(xmlPrivateKey);
        }

        public static byte[] EncryptText(string text, string xmlPublicKey)
        {
            RSACryptoServiceProvider RSA = new RSACryptoServiceProvider(2048);
            RSA.FromXmlString(xmlPublicKey);
            byte[] encryptedData = RSA.Encrypt(Encoding.ASCII.GetBytes(text), false);
            return encryptedData;
        }

        public static byte[] DecryptText(string encrypttext, string xmlPrivateKey)
        {
            RSACryptoServiceProvider RSA = new RSACryptoServiceProvider(2048);
            RSA.FromXmlString(xmlPrivateKey);
            byte[] decryptData = RSA.Decrypt(Convert.FromBase64String(encrypttext), false);
            return decryptData;
        }
    }
}
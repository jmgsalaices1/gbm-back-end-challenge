﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Web;
using WHService.Application;
using WHService.Contract.Data.HistoricTrip;
using WHService.Contract.Data.Trip;
using WHService.DataAccess;

namespace WHService.Application
{
    public class AppManager : IDisposable
    {
        private readonly IMapper _mapper;
        public AppManager()
        {
            this._mapper = AppProfile.ConfigMaps();
        }

        #region Trip

        public GetHistoricTripsResponse GetHistoricTrips(GetHistoricTripRequest request)
        {
            GetHistoricTripsResponse response = new GetHistoricTripsResponse();
            List<Domain.HistoricTrip> a = HistoricTripRepository.GetHistoricTripList();
            response.HistoricTrips = this._mapper.Map<List<Domain.HistoricTrip>, List<GetHistoricTripResponse>>(a);
            return response;
        }

        public GetHistoricTripsResponse GetHistoricTripByTripId(GetHistoricTripRequest request)
        {
            GetHistoricTripsResponse response = new GetHistoricTripsResponse();
            var a = HistoricTripRepository.GetHistorcTripById(request.TripId);
            response.HistoricTrips = this._mapper.Map<List<Domain.HistoricTrip>, List<GetHistoricTripResponse>>(a);
            return response;
        }

        public AddHistoricTripResponse AddHistoricTrip(AddHistoricTripRequest request)
        {
            AddHistoricTripResponse response = new AddHistoricTripResponse();
            Domain.HistoricTrip a = new Domain.HistoricTrip();
            
            a.TripId = Convert.ToInt32(request.TripId);
            a.Location = DbGeography.FromText(request.Location.ToString());
            a.StatusTrip = request.StatusTrip;
            a.UpdateDate = DateTime.Now;
            HistoricTripRepository.Add(a);

            return response;
        }

        #endregion

        #region HistoricTrip
        public GetTripsResponse GetTrips(GetTripRequest request)
        {
            GetTripsResponse response = new GetTripsResponse();
            List<Domain.Trip> a = TripRepository.GeTripList();
            response.Trips = this._mapper.Map<List<Domain.Trip>, List<GetTripResponse>>(a);
            return response;
        }

        public GetTripResponse GetTrip(GetTripRequest request)
        {
            GetTripResponse response = new GetTripResponse();
            Domain.Trip a = TripRepository.GetTripById(request.TripId);
            response = this._mapper.Map<Domain.Trip, GetTripResponse>(a);
            return response;
        }

        public AddTripResponse AddTrip(AddTripRequest request)
        {
            AddTripResponse response = new AddTripResponse();
            Domain.Trip a = new Domain.Trip();

            a.TaxiId = Convert.ToInt32(request.TaxiId);
            a.ClientId = Convert.ToInt32(request.ClientId);
            a.Status = Convert.ToByte(request.Status);
            a.StartPoint = DbGeography.FromText(request.StartPoint.ToString());
            a.EndPoint = DbGeography.FromText(request.EndPoint.ToString());
            a.CreateDate = DateTime.Now;
            a.UpdateDate = DateTime.Now;

            TripRepository.Add(a);
            return response;
        }

        #endregion

        #region Application
        //public GetApplicationsResponse GetApplications(GetApplicationRequest request)
        //{
        //    GetApplicationsResponse response = new GetApplicationsResponse();
        //    List<Domain.Application> a = ApplicationRepository.GetApplications();
        //    response.Applications = this._mapper.Map<List<Domain.Application>, List<GetApplicationResponse>>(a);
        //    return response;
        //}

        //public GetApplicationResponse GetApplication(GetApplicationRequest request)
        //{
        //    GetApplicationResponse response = new GetApplicationResponse();
        //    Domain.Application a = ApplicationRepository.GetApplicationByName(request.ApplicationName);
        //    response = this._mapper.Map<Domain.Application, GetApplicationResponse>(a);
        //    return response;
        //}

        //public AddApplicationResponse AddApplication(AddAplicationRequest request)
        //{
        //    AddApplicationResponse response = new AddApplicationResponse();
        //    Domain.Application a = new Domain.Application();
        //    a.ApplicationName = request.ApplicationName;
        //    a.ApiKey = Guid.NewGuid().ToString();
        //    CryptoHelper cipher = new CryptoHelper();
        //    a.PrivateKey = Convert.ToBase64String(cipher.CreatePrivateKey());
        //    a.PublicKey = Convert.ToBase64String(cipher.CreatePublicKey());
        //    ApplicationRepository.Add(a);
        //    return response;
        //}

        //public UpdateApplicationResponse UpdateApplication(UpdateApplicationRequest request)
        //{
        //    UpdateApplicationResponse response = new UpdateApplicationResponse();
        //    Domain.Application a = ApplicationRepository.GetApplicationById(request.CveApplication);
        //    a.ApplicationName = request.ApplicationName;
        //    ApplicationRepository.Update(a);
        //    return response;
        //}

        //public DeleteApplicationResponse DeleteApplication(DeleteApplicationRequest request)
        //{
        //    DeleteApplicationResponse response = new DeleteApplicationResponse();
        //    ApplicationRepository.Delete(request.CveApplication);
        //    return response;
        //}

        #endregion

        #region Message
        //public GetMessagesResponse GetMessages(GetMessageRequest request)
        //{
        //    GetMessagesResponse response = new GetMessagesResponse();
        //    List<Domain.Message> m = MessageRepository.GetMessagesByApplicationName(request.ApplicationName);
        //    response.Messages = this._mapper.Map<List<Domain.Message>, List<GetMessageResponse>>(m);
        //    return response;
        //}

        //public GetMessageResponse GetMessage(GetMessageRequest request)
        //{
        //    GetMessageResponse response = new GetMessageResponse();
        //    Domain.Message m = MessageRepository.GetMessagesById(request.CveMessage);
        //    response = this._mapper.Map<Domain.Message, GetMessageResponse>(m);
        //    return response;
        //}

        //public AddMessageResponse AddMessage(AddMessageRequest request)
        //{
        //    AddMessageResponse response = new AddMessageResponse();
        //    Domain.Message m = new Domain.Message();
        //    m.CveApplication = request.CveApplication;
        //    m.MessageName = request.MessageName;
        //    CustomCompiler.EvalCode(request.RequestData);
        //    CustomCompiler.EvalCode(request.ResponseData);
        //    m.RequestData = request.RequestData;
        //    m.ResponseData = request.ResponseData;
        //    m.ResourceName = request.ResourceName;
        //    MessageRepository.Add(m);
        //    return response;
        //}

        //public UpdateMessageResponse UpdateMessage(UpdateMessageRequest request)
        //{
        //    UpdateMessageResponse response = new UpdateMessageResponse();
        //    Domain.Message m = new Domain.Message();
        //    m.CveApplication = request.CveApplication;
        //    m.CveMessage = request.CveMessage;
        //    m.MessageName = request.MessageName;
        //    CustomCompiler.EvalCode(request.RequestData);
        //    CustomCompiler.EvalCode(request.ResponseData);
        //    m.RequestData = request.RequestData;
        //    m.ResponseData = request.ResponseData;
        //    m.ResourceName = request.ResourceName;
        //    MessageRepository.Update(m);
        //    return response;
        //}

        //public DeleteMessageResponse DeleteMessage(DeleteMessageRequest request)
        //{
        //    DeleteMessageResponse response = new DeleteMessageResponse();
        //    MessageRepository.Delete(request.CveMessage);
        //    return response;
        //}

        #endregion

        #region Data

        //public GetDatasResponse GetDatas(GetDataRequest request)
        //{
        //    GetDatasResponse response = new GetDatasResponse();
        //    List<Domain.Data> d = DataRepository.GetDataByApplicationName(request.ApplicationName);
        //    response.Datas = this._mapper.Map<List<Domain.Data>, List<GetDataResponse>>(d);
        //    return response;
        //}

        //public GetDataResponse GetData(GetDataRequest request)
        //{
        //    GetDataResponse response = new GetDataResponse();
        //    Domain.Data d = DataRepository.GetDataById(request.CveData);
        //    response = this._mapper.Map<Domain.Data, GetDataResponse>(d);
        //    return response;
        //}

        #endregion
        public void Dispose()
        {

        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TaxiBackEnd.Application.Main.Interfaces;
using TaxiBackEnd.Domain.Entities;
using TaxiBackEnd.Domain.Interfaces.Repositories;

namespace TaxiBackEnd.Presentation.WebAPI.Controllers
{
    [AllowAnonymous]
    [RoutePrefix("api/trip")]
    public class HistoricTripController : ApiController
    {
        private readonly IBaseAppService<HistoricTrip> repository;

        public HistoricTripController(IBaseAppService<HistoricTrip> repository)
        {
            this.repository = repository;
        }

        [HttpGet]
        [Route("HistoricTrip")]
        public IEnumerable<HistoricTrip> Get()
        {
            var listHistoric = this.repository.getAll();
            return listHistoric;
           
        }

        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            HistoricTrip answer = this.repository.getById(id); 

            if (answer.HistoricId != 0)
            {
                return Request.CreateResponse(answer); 
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Invalid id."); // Se retorna un mensaje de error.
            }
        }


        /// <summary>
        /// POST: api/HistoricTrip
        /// Método que permite agregar nuevo historico de la ubiación(Geograpy/Locaion) en tiempo real un viaje(trip) al recibir una petición POST.
        /// </summary>
        /// <param name="tripid">Corresponde al identificador del viaje.</param>
        /// <param name="longitude">Corresponde a la longitude geografica.</param>
        /// <param name="latitude">Corresponde a la latitude geografica.</param>
        /// <param name="statustrip">Corresponde al status del viaje.</param>
        /// <returns><c>HttpResponseMessage</c>: Respuesta HTTP.</returns>
        [HttpPost]
        public HttpResponseMessage Post(int tripId, string longitude, string latitude, string statustrip)
        {

            if (tripId > 0 && longitude?.Length > 0 && latitude?.Length > 0)
            {
                var answer = new HistoricTrip
                {
                    TripId = tripId,
                    Location = DbGeography.FromText($"POINT({Convert.ToDouble(latitude)} {Convert.ToDouble(longitude)})"),
                    StatusTrip = 1,
                    UpdateDate = DateTime.Now
                };

                try
                {
                    this.repository.add(answer);
                    return Request.CreateResponse(answer);
                }
                catch (Exception)
                {

                    return this.getMessage(1);
                }

            }
            else
            {
                return this.getMessage(1);
            }
        }

        /// <summary>
        /// Método que retorna un mensaje HTTP.
        /// </summary>
        /// <param name="id">Corresponde al identificador del mensaje.</param>
        /// <returns><c>HttpResponseMessage</c>: Respuesta HTTP.</returns>
        private HttpResponseMessage getMessage(Int32 id)
        {
            switch (id)
            {
                case 0: return Request.CreateResponse("Executed correctly!");
                case 1: return Request.CreateErrorResponse(HttpStatusCode.Conflict, "You not specified all the infomation for Historic."); // Se retorna un mensaje de error.;
                case 2: return Request.CreateErrorResponse(HttpStatusCode.Conflict, "The HistoricID already exist."); // Se retorna un mensaje de error.;
                default: return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Invalid id."); // Se retorna un mensaje de error.;
            }
        }

        public static DbGeography ConvertLatLonToDbGeography(double longitude, double latitude)
        {
            var point = string.Format("POINT({1} {0})", latitude, longitude);
            return DbGeography.FromText(point);
        }
    }
}

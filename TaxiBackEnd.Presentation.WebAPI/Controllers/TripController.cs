﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TaxiBackEnd.Application.Main.Interfaces;
using TaxiBackEnd.Domain.Entities;

namespace TaxiBackEnd.Presentation.WebAPI.Controllers
{
    public class TripController : ApiController
    {
        private readonly ITripAppService repository;

        public TripController(ITripAppService repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// GET: api/Trip
        /// </summary>
        /// <returns><c>HttpResponseMessage</c>: Respuesta HTTP.</returns>
        [HttpGet]
        public HttpResponseMessage Get()
        {
            List<Trip> listHistoric = this.repository.getAll().ToList();

            // Se valida si existen empresas en la base de datos.
            if (listHistoric?.Count() > 0)
            {
                return Request.CreateResponse(listHistoric); 
            }// Fin del if.
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "There are not Trip Historic."); // Se retorna un mensaje de error.
            }// Fin del else.
        }// Fin del método.

        /// <summary>
        /// GET: api/Trip/5
        /// </summary>
        /// <param name="id">Corresponde al identificador de la Trip.</param>
        /// <returns><c>HttpResponseMessage</c>: Respuesta HTTP.</returns>
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            Trip answer = this.repository.getById(id); 

            if (answer.TripId != 0)
            {
                return Request.CreateResponse(answer);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Invalid id."); // Se retorna un mensaje de error.
            }
        }


        /// <summary>
        /// POST: api/Trip
        /// Método que permite agregar nuevos TRIPs.
        /// api/Company
        /// </summary>
        /// <param name="taxi">Corresponde identificador del taxi.</param>
        /// <param name="client">Corresponde identificador del cliente.</param>
        /// <param name="start">Corresponde el punto iniciol de Trip.</param>
        /// <param name="end">Corresponde a la punto final.</param>
        /// <returns><c>HttpResponseMessage</c>: Respuesta HTTP.</returns>
        [HttpPost]
        public HttpResponseMessage Post(string taxi, string client, string start, string end)
        {
            if (taxi?.Length > 0 && client?.Length > 0 && start?.Length > 0 && end?.Length > 0)
            {
                var trip = new Trip()
                {
                    TripId = this.repository.getAll().ToList().Count + 1,
                    ClientId = Convert.ToInt32(client),
                    CreateDate = DateTime.Now,
                    Status = 0,
                    StartPoint = System.Data.Entity.Spatial.DbGeography.FromText($"POINT({start})"),
                    EndPoint = System.Data.Entity.Spatial.DbGeography.FromText($"POINT({end})"),
                    UpdateDate = DateTime.Now,
                    TaxiId = Convert.ToInt32(taxi)
                };
                this.repository.add(trip);

                return this.getMessage(1);
            }
            else
            {
                return this.getMessage(1);
            }
        }


        /// <summary>
        /// Método que retorna un mensaje HTTP.
        /// </summary>
        /// <param name="id">Corresponde al identificador del mensaje.</param>
        /// <returns><c>HttpResponseMessage</c>: Respuesta HTTP.</returns>
        private HttpResponseMessage getMessage(Int32 id)
        {
            switch (id)
            {
                case 0: return Request.CreateResponse("Executed correctly!");
                case 1: return Request.CreateErrorResponse(HttpStatusCode.Conflict, "You not specified all the infomation for Historic."); // Se retorna un mensaje de error.;
                case 2: return Request.CreateErrorResponse(HttpStatusCode.Conflict, "The HistoricID already exist."); // Se retorna un mensaje de error.;
                case 3: return Request.CreateErrorResponse(HttpStatusCode.Conflict, "METHOD NOT IMPLEMENT."); // Se retorna no implementado.;
                default: return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Invalid id."); // Se retorna un mensaje de error.;
            }
        }
    }
}

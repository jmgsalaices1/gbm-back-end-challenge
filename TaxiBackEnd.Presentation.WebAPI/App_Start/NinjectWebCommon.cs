[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(TaxiBackEnd.Presentation.WebAPI.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(TaxiBackEnd.Presentation.WebAPI.App_Start.NinjectWebCommon), "Stop")]

namespace TaxiBackEnd.Presentation.WebAPI.App_Start
{
    using System;
    using System.Web;
    using System.Web.Http;

    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;
    using TaxiBackEnd.Application.Main.Interfaces;
    using TaxiBackEnd.Application.Main.Services;
    using TaxiBackEnd.Domain.Core;
    using TaxiBackEnd.Domain.Interfaces.Repositories;
    using TaxiBackEnd.Domain.Interfaces.Services;
    using TaxiBackEnd.Infraestructure.Data.Repositories;

    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            
            var kernel = new StandardKernel();
            try
            {
                //GlobalConfiguration.Configuration.DependencyResolver = new NinjectDependencyResolver(kernel);
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            
            //Servicios de la capa de aplicación
            kernel.Bind(typeof(IBaseAppService<>)).To(typeof(BaseAppService<>));
            kernel.Bind<ITripAppService>().To<ITripAppService>();
            kernel.Bind<IHistoricTripService>().To<IHistoricTripService>();

            //Servicios de la capa de aplicación con servicios de dominio
            kernel.Bind(typeof(IBaseService<>)).To(typeof(BaseService<>));
            kernel.Bind<ITripService>().To<TripService>();
            kernel.Bind<IHistoricTripService>().To<HistoricTripService>();

            //Repositorio del dominio a los de infraestructura
            kernel.Bind(typeof(IBaseRepository<>)).To(typeof(BaseRepository<>));
            kernel.Bind<ITripRepository>().To<TripRepository>();
            kernel.Bind<IHistoricTripRepository>().To<HistoricTripRepository>();
        }        
    }
}

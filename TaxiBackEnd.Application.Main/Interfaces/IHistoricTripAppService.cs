﻿using TaxiBackEnd.Domain.Entities;

namespace TaxiBackEnd.Application.Main.Interfaces
{
    /// <summary>
    /// Interfaz que contiene los métodos para la entidad de HistoricTrip, e implementa la interfaz IBaseAppService.
    /// </summary>
    public interface IHistoricTripAppService : IBaseAppService<HistoricTrip>
    {
    }
}

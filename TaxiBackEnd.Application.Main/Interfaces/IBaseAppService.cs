﻿using System.Collections.Generic;

namespace TaxiBackEnd.Application.Main.Interfaces
{
    /// <summary>
    /// Interface generica y que puede ser utilizado por cualquier entidad.
    /// </summary>
    public interface IBaseAppService<TEntity> where TEntity: class
    {
        /// <summary>
        /// Método que permite ingresar una entidad
        /// </summary>
        /// <param name="entity">Corresponde a la entidad que se desea agregar</param>
        void add(TEntity entity);

        /// <summary>
        /// Método que permite eliminar una entidad
        /// </summary>
        /// <param name="id">Corresponde al identificador de la entidada que se desea eliminar</param>
        void delete(int id);

        /// <summary>
        /// Método que permite actualizar la información una entidad
        /// </summary>
        /// <param name="entity">Corresponde a la entidad que se desea actualizar</param>
        void modify(TEntity entity);

        /// <summary>
        /// Método que permite obtener todos los registros pertenecientes a esa entidad
        /// </summary>
        /// <returns>Collección de objetos obtenidos de entidad</returns>
        IEnumerable<TEntity> getAll();

        /// <summary>
        /// Método que permite obtener la información correspondiente a la entidad solicitada
        /// </summary>
        /// <param name="id">Identificador de la entidad</param>
        /// <returns>entidad ontenida de la consulta</returns>
        TEntity getById(int id);

        void Dispose();
    }
}

﻿using TaxiBackEnd.Domain.Entities;

namespace TaxiBackEnd.Application.Main.Interfaces
{
    /// <summary>
    /// Interfaz que contiene los métodos para la entidad de Trip, e implementa la interfaz IBaseAppService.
    /// </summary>
    public interface ITripAppService : IBaseAppService<Trip>
    {
    }
}

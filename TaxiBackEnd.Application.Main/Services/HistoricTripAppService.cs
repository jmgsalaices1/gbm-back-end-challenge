﻿using TaxiBackEnd.Domain.Entities;
using TaxiBackEnd.Domain.Interfaces.Services;
using TaxiBackEnd.Application.Main.Interfaces;

namespace TaxiBackEnd.Application.Main.Services
{
    /// <summary>
    /// Clase que contiene los métodos de la entidad HistoricTrip, 
    /// se encarga de realizar la conexión entre los servicios de la aplicación con los servicios del dominio.
    /// </summary>
    public class HistoricTripAppService: BaseAppService<HistoricTrip>, IHistoricTripAppService
    {
        private readonly IHistoricTripService historicTripSevice;

        public HistoricTripAppService(IHistoricTripService historicTripSevice):base(historicTripSevice)
        {
            this.historicTripSevice = historicTripSevice;
        }
    }
}

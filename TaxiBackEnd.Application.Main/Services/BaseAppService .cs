﻿using System;
using System.Collections.Generic;
using TaxiBackEnd.Application.Main.Interfaces;
using TaxiBackEnd.Domain.Interfaces.Services;

namespace TaxiBackEnd.Application.Main.Services
{
    /// <summary>
    /// Clase genérica que puede ser utilizado por cualquier entidad, 
    /// se encarga de realizar la conexión 
    /// entre los servicios de la capa de aplicación 
    /// con los servicios del dominio.
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public class BaseAppService<TEntity> : IDisposable, IBaseAppService<TEntity> where TEntity : class
    {
        private readonly IBaseService<TEntity> baseService;

        public BaseAppService(IBaseService<TEntity> baseService)
        {
            this.baseService = baseService;
        }

        public void add(TEntity entity)
        {
            this.baseService.add(entity);
        }

        public void delete(int id)
        {
            this.baseService.delete(id);
        }

        public void Dispose()
        {
            this.baseService.Dispose();
        }

        public IEnumerable<TEntity> getAll()
        {
            return this.baseService.getAll();
        }

        public TEntity getById(int id)
        {
            return this.baseService.getById(id);
        }

        public void modify(TEntity entity)
        {
            this.baseService.modify(entity);
        }
    }
}

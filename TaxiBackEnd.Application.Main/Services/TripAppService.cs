﻿using TaxiBackEnd.Domain.Entities;
using TaxiBackEnd.Domain.Interfaces.Services;
using TaxiBackEnd.Application.Main.Interfaces;

namespace TaxiBackEnd.Application.Main.Services
{
    /// <summary>
    /// Clase que contiene los métodos de la entidad Trip, 
    /// se encarga de realizar la conexión entre los servicios de la aplicación con los servicios del dominio.
    /// </summary>
    public class TripAppService: BaseAppService<Trip>, ITripAppService
    {
        private readonly ITripService tripAppService;

        public TripAppService(ITripService tripAppService) : base(tripAppService)
        {
            this.tripAppService = tripAppService;
        }
    }
}
